﻿using Calculation.Interfaces;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text;

namespace Calculation.Implementation
{
    public class SerilogLogger : Calculation.Interfaces.ILogger
    {
        public SerilogLogger() 
        {
            Log.Logger = new LoggerConfiguration()
        .WriteTo.Console()
        .CreateLogger();

        }
        public void Error(Exception ex)
        {
            Log.Error(ex, ex.Message);
        }

        public void Info(string message)
        {
            Log.Information(message);
        }

        public void Trace(string message)
        {
            Log.Debug(message);
        }
    }
}
