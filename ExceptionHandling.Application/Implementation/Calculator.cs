﻿using System;
using System.Linq;
using Calculation.Interfaces;

namespace Calculation.Implementation
{
	public class Calculator : BaseCalculator, ICalculator
	{
		private ILogger _logger;
		public Calculator(ILogger logger)
		{
			_logger = logger;
		}

		public int Sum(params int[] numbers)
		{
			try
			{
				_logger.Trace($"{nameof(Calculator.Sum)}");				
				var sum = SafeSum(numbers);
				_logger.Info($"{nameof(Calculator.Sum)} {string.Join(',', numbers)} = {sum}");
				return sum;
			}
			catch (Exception ex)
			{
				_logger.Error(ex);
				throw;				
			}
			finally 
			{
				_logger.Trace($"{nameof(Calculator.Sum)} {string.Join(',', numbers)}");				
			}
           
		}

		public int Sub(int a, int b)
		{
            try
            {
				return SafeSub(a, b);
			}
            catch (System.Exception e)
            {

                throw e;
            }
            
		}

		public int Multiply(params int[] numbers)
		{
            try
            {
             if (!numbers.Any())
				return 0;

			return SafeMultiply(numbers);
            }
            catch (System.Exception e)
            {

                throw new InvalidOperationException(e.Message,e);    
            }
            
		}

		public int Div(int a, int b)
		{
            try
            {
				return a / b;
			}
            catch (Exception)
            {

				throw new InvalidOperationException();
			}			
		}
	}
}