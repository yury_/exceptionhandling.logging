﻿using Calculation.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Calculation.Implementation
{    
    internal class ConsoleLogger : ILogger
    {
        public void Error(Exception ex)
        {
            Console.WriteLine($"Error {ex.Message}");
        }

        public void Info(string message)
        {
            Console.WriteLine($"Info {message}");
        }

        public void Trace(string message)
        {
            Console.WriteLine($"Trace {message}");
        }
    }
}
